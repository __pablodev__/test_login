package com.pablojmuratore.testlogin2.hilt

import com.pablojmuratore.testlogin2.login.ILoginRepository
import com.pablojmuratore.testlogin2.login.LoginRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@InstallIn(ViewModelComponent::class)
@Module
class LoginModule {
    @ViewModelScoped
    @Provides
    fun provideLoginRepository(): ILoginRepository {
        return LoginRepository()
    }
}