package com.pablojmuratore.testlogin2

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TestLoginApp : Application() {
}