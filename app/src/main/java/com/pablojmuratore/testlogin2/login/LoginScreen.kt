package com.pablojmuratore.testlogin2.login

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.pablojmuratore.testlogin2.R

@Composable
fun LoginScreen() {
    val viewModel = hiltViewModel<LoginScreenViewModel>()
    val name = viewModel.name.collectAsState().value
    val password = viewModel.password.collectAsState().value
    val validationSuccess = viewModel.validationSuccess.collectAsState().value

    when (validationSuccess) {
        ValidationEnums.NOT_VALIDATED, ValidationEnums.VALIDATION_ERROR -> {
            Column() {
                if (validationSuccess == ValidationEnums.VALIDATION_ERROR) {
                    Text(text = stringResource(id = R.string.validation_error_message))

                    Spacer(modifier = Modifier.height(16.dp))
                }

                Text(stringResource(id = R.string.enter_name))
                TextField(value = name, onValueChange = { viewModel.onNameChanged(it) })
                Text(stringResource(id = R.string.enter_name))
                TextField(value = password, onValueChange = { viewModel.onPasswordChanged(it) })

                Button(
                    onClick = { viewModel.validate() }
                ) {
                    Text(text = stringResource(id = R.string.validate))
                }
            }
        }

        ValidationEnums.VALIDATION_SUCCESS -> {
            Surface {
                Text(text = stringResource(id = R.string.validation_success))
            }
        }
    }
}