package com.pablojmuratore.testlogin2.login

enum class ValidationEnums {
    NOT_VALIDATED,
    VALIDATION_SUCCESS,
    VALIDATION_ERROR
}