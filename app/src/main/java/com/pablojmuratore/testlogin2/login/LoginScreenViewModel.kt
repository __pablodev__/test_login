package com.pablojmuratore.testlogin2.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginScreenViewModel @Inject constructor(
    val loginRepository: ILoginRepository
) : ViewModel() {
    private val _name = MutableStateFlow<String>("")
    val name: StateFlow<String> = _name

    private val _password = MutableStateFlow<String>("")
    val password: StateFlow<String> = _password

    private val _validationSuccess = MutableStateFlow<ValidationEnums>(ValidationEnums.NOT_VALIDATED)
    val validationSuccess = _validationSuccess

    fun onNameChanged(newName: String) {
        _name.value = newName
    }

    fun onPasswordChanged(newPassword: String) {
        _password.value = newPassword
    }

    fun validate() {
        viewModelScope.launch(Dispatchers.IO) {
            val validationResult = loginRepository.validateLogin(_name.value, password.value)

            _validationSuccess.value = when (validationResult) {
                true -> ValidationEnums.VALIDATION_SUCCESS
                else -> ValidationEnums.VALIDATION_ERROR
            }
        }
    }
}