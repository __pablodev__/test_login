package com.pablojmuratore.testlogin2.login

interface ILoginRepository {
    suspend fun validateLogin(name: String, password: String): Boolean
}